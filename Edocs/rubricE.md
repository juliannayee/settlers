The project is to create a settlers of Notre Dame board game. This requires a graphic representation of the board for the user to interact with. 

RUBRIC:
=======

The lessons 1-4, 6 and 8 on http://lazyfoo.net/tutorials/SDL/index.php will likely be beneficial. 

+30 pts for these lessons (5 for each lesson).

+10 pts for making one of the board pieces that we will use. Just displaying the piece on the screen.
