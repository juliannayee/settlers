//Erin Bradford, ebradfo2, piece.h
//Piece header file

#ifndef PIECE_H
#define PIECE_H

#include <vector>
#include <string>

using namespace std;

class Piece{

  public:
    Piece(int,string); // instantiate piece with roll and type
    int getRoll() const; // access what roll value is 
    string getType() const; // what is this type
    void print(); // print out what is here
  private:
    int roll; // number rolled to get this piece
    string type; //what resource do you get here?
    
};
#endif
