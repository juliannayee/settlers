// Erin Bradford, player.cpp
// functions from player class implemented

#include <map>
#include <iostream>
#include "Player.h"
#include "Piece.h"
#include <string>

using namespace std;

// Constructor function
Player::Player() {
  totcards=0; //set variables to zero (or false)
  vpoints=0; 
  cards["sun"] =0;
  cards["bicep"] =0;
  cards["test"] =0;
  cards["berry"] =0;
  cards["helmet"] =0;
  numPieces=0;
  leps=0;
  lep=false;
};

//player draws card
void Player::draw(string type) {
  cards[type] += 1; // add card of type to map
  totcards += 1;  // update total card count
  //cout << "drawing " << type << " total is : " << totcards << endl;
}

//player uses a card and it leaves hand
void Player::play(string type) {
  cards[type] -= 1; // remove card of type from map
	totcards -=1; // update total card count
}

void Player::build(Piece boardpiece) { // add piece to players hand when they build
  pieces.push_back(boardpiece);
}

//player loses half of all cards if they have 7 or more cards when a 7 is rolled
void Player::rob(){
  if (totcards>=7) { //do they have 7 or more cards
    int sum = 0; //used to calculate new total cards
    string type;
    map<string, int>::const_iterator it; //instantiate iterator
    for (it = cards.begin(); it != cards.end(); ++it) { // move iterator through map
      type= it->first; //figure out which card is here
      cards[type] = cards[type]/2; //half the number of cards of this type they have, goes in favor of player when odd
      sum = cards[type] +sum; //add remaining cards to total
    }
    totcards = sum; //update total
    //debugging
    //cout << "Total Cards: " << totcards << endl; 
  }
}

//player gets victory points for building quad or dorm or development cards
void Player::addpoints() {
  vpoints++; // add points
}

int Player::getvpoints() { // return number of vpoints one has
  if (lep) return vpoints+1; // largest army is a movable victory point - if you have it you have one extra vpoint
  else return vpoints;
}

//determinne if player has won
bool Player::checkWin(int numPlayers) {
  int num2win;
  switch(numPlayers) { //v points needed depends on number of players
    case 2: num2win = 10; break; 
    case 3: num2win = 8; break;
    case 4: num2win = 6; break;
  }
  if (getvpoints() >= num2win)  //if you have enough victory points you win
    return true;
  else
    return false;
}

bool Player::checkResources(string building) { //make sure player has resources required to build
  if (building == "sidewalk") {
    if (cards["bicep"]>=1 && cards["test"]>=1 && cards["berry"]>=1)
      return true;
    else {
      //debugging
      //print(); //make sure don't have enough pieces
      return false; 
    }
  }
  else if (building == "dorm") {
    if (cards["test"]>=1 && cards["bicep"]>=1 && cards["helmet"]>=1 && cards["berry"]>=1)
      return true;
    else {
      //debugging
      //print(); //make sure don't have enough pieces
      return false;     
    }
  }
  else if (building == "quad"){ //string == quad
    if (cards["sun"]>=3 && cards["helmet"]>=2)
      return true;
    else {  
      //debugging
      //print(); //make sure don't have enough pieces
      return false;     
    }
  }
  else { //need sun berry helmet for development card
    if (cards["sun"]>=1 && cards["berry"]>=1 && cards["helmet"]>=1)
      return true;
    else
      return false;

  }
}

void Player::removeResources(string building) { // update resource cards after building
  if (building == "sidewalk") { //sidewalk building
    cards["bicep"]--;
    cards["test"]--;
    cards["berry"]--;
    totcards = totcards - 3;
  }
  else if (building == "dorm") { //dorm building
    cards["test"]--;
    cards["bicep"]--;
    cards["helmet"]--;
    cards["berry"]--;
    totcards = totcards - 4;
  }
  else if (building == "quad") { //string == quad
    cards["sun"] = cards["sun"] - 3;
    cards["helmet"] = cards["helmet"] - 2;
    totcards = totcards - 5;
  }
  else if (building == "development") { //development card
    cards["sun"]=cards["sun"]-1;
    cards["berry"]=cards["berry"]-1;
    cards["helmet"]=cards["helmet"]-1;
    totcards = totcards-3;
  }
}

bool Player::hasPiece(int roll) { //decide whether or not to give card
  for ( int i = 0; i < numPieces; i++ ) { //loop through pieces player is on
    if (pieces[i].getRoll() == roll) {//if roll matches piece roll
      draw(pieces[i].getType()); //give one resource card to player
    }
  }
  //debugging
  //print(); //print player's current hand
}

void Player::tradeResource() {
  //player can trade in 3 of a kind for 1 resource
  int choice;
  string resource, trade;
   
	//prompt user
  cout << "What resource would you like to trade in? You must have three of a kind to trade!" << endl;
  cout << " 	1 - Berry" << endl;
  cout << " 	2 - Bicep" << endl;
  cout << " 	3 - Helmet" << endl;
  cout << " 	4 - Sun" << endl;
  cout << " 	5 - Test" << endl;
  cout << "Please select a resource (1-5): ";
  cin >> choice;

	//check for valid input
  while (choice<1 || choice>=6) {
    cout << "Please select an option 1-5: ";
    cin >> choice;
  }
	
	//choice made convert num to string
  resource = assignChoice(choice);
	
	//prompt user
  if (cards[resource]>=3) {
    cout << "Ok. What resource would you like in exchange?" << endl;
    cout << " 	1 - Berry" << endl;
    cout << " 	2 - Bicep" << endl;
    cout << " 	3 - Helmet" << endl;
    cout << " 	4 - Sun" << endl;
    cout << " 	5 - Test" << endl;
    cout << "Please select a resource (1-5): ";
    cin >> choice;

		//check for valid input
    while (choice<1 || choice>=6) {
      cout << "Please select an option 1-5: ";
      cin >> choice;
    }

		// num to string
    trade = assignChoice(choice);

    //trade cards
    cout << "Trade 3 " << resource << " cards for 1 " << trade << "!" << endl;
    cards[resource] = cards[resource]-3;
    cards[trade]++;
    totcards = totcards-2;
  }
  //jmake sure trade is possible
  else {
    cout << "You do not have 3 " << resource << " cards!" << endl;
  }
}

//helper function to assign choice in tradeResource() function
string Player::assignChoice(int choice) {
  string resource;
  if (choice == 1) 
    resource = "berry";
  else if (choice == 2)
    resource = "bicep";
  else if (choice == 3) 
    resource = "helmet";
  else if (choice == 4)
    resource = "sun";
  else
    resource = "test";
  return resource;
}

//print out the players hand
void Player::print(){

  map<string, int>::const_iterator it; //instantiate iterator
  for (it = cards.begin(); it != cards.end(); ++it) {// move iterator through map
    if (it  == cards.begin()) // formatting, commas before all but first term
      cout << it->first << ":" << it->second;
    else
      cout << ", " << it->first << ":" << it->second;
  }
  cout << endl;
//  cout << " total cards: " << totcards << endl;
}

void Player::printPieces() { //debug - show all pieces
  for ( int i = 0; i < numPieces; i++ )  //loop through pieces
    pieces[i].print();
}

int Player::stealCards(string type) { //used with monopoly steals type from player
  int num = cards[type]; //get value of type of card
  cards[type]=0; //reset type to zero
  totcards = totcards - num; //update total cards
  return num; //determine home many of card to give to other player
}

void Player::addLep(){ // add to leprechaun army
  leps += 1;
}

void Player::setLep(bool opt){ //change status of army card
  lep = opt;
}

