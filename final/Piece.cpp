// Piece.cpp
// functions from piece class implemented

#include <iostream>
#include <vector>
#include <string>
#include "Piece.h"

using namespace std;

// Constructor function
Piece::Piece(int r, string t) : roll(r), type(t) {} //set variables


int Piece::getRoll() const{ //get value of roll 
  return roll;
}
	
string Piece::getType() const{ // get type stored here
  return type;
}


void Piece::print() { // see what this piece is
  cout << "Roll: " << roll << ", Resource: " << type << endl;
}




  
