//Using SDL, SDL_image, standard IO, and strings
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <iostream>
#include "Graphics.h"
#include <vector>
#include <string>
using namespace std;

//this function will initialize the game, up to the part when the user presses play game
Graphics::Graphics() {

  //initialize the leprechaun so it starts on the lakes
  lepPos=0;
 
  //initialize window and screens  
  gWindow = NULL;
  gScreenSurface = NULL;
  gCurrentSurface = NULL;
  currentBoard = NULL;  
}

//function to draw dorm at the specified location
void Graphics::addDorm(int x, int y, int player) {
  int xn = xPos(x);
  int yn = yPos(y);
  string dorm;
  switch(player){
    case 1:
     dorm = "images/DP1.jpg";
     break;
    case 2:
     dorm = "images/DP2.jpg";
     break;
    case 3:
     dorm = "images/DP3.jpg";
     break;
    case 4:
     dorm = "images/DP4.jpg";
     break;
  }
  SDL_Rect Rect = {xn,yn,30,30};
  SDL_Rect Rect2 = {xn-100,yn-10,30,30};
  SDL_BlitSurface(IMG_Load(dorm.c_str()), NULL, currentBoard, &Rect2);
  SDL_BlitSurface(IMG_Load(dorm.c_str()), NULL, gScreenSurface, &Rect);
  SDL_UpdateWindowSurface(gWindow);
  addLep(); 

}

//function to draw sidewalk at the specified location
void Graphics::addSide(int x, int y, int player){
  int xn = xPos(x);
  int yn = yPos(y);
  int y2,x2;
  string side;
  if(xn>0&&yn==0) {
    y2 = yPos2(y);
    SDL_Rect Rect = {xn+8,y2,15,50};
    SDL_Rect Rect2 = {xn-92,y2-10,15,50};
    switch(player) {
      case 1:
        side = "images/P1sidev.jpg";
        break;
      case 2:
        side = "images/P2sidev.jpg";
        break;
      case 3:
        side = "images/P3sidev.jpg";
        break;
      case 4:
        side = "images/P4sidev.jpg";
        break;
    }
    SDL_BlitSurface(IMG_Load(side.c_str()), NULL, currentBoard, &Rect2);
    SDL_BlitSurface(IMG_Load(side.c_str()), NULL, gScreenSurface, &Rect);
    SDL_UpdateWindowSurface(gWindow);
  }
  else{
    x2 = xPos2(x);
    SDL_Rect Rect = {x2,yn+8,50,15};
    SDL_Rect Rect2 = {x2-100,yn-2,50,15};
    switch(player) {
      case 1:
        side = "images/P1sideh.jpg";
        break;
      case 2:
        side = "images/P2sideh.jpg";
        break;
      case 3:
        side = "images/P3sideh.jpg";
        break;
      case 4:
        side = "images/P4sideh.jpg";
        break;
    }
    SDL_BlitSurface(IMG_Load(side.c_str()), NULL, currentBoard, &Rect2);
    SDL_BlitSurface(IMG_Load(side.c_str()), NULL, gScreenSurface, &Rect);
    SDL_UpdateWindowSurface(gWindow);
  }
  addLep();
}

//draw a quad at the specified location
void Graphics::addQuad(int x, int y, int player) {
  int xn = xPos(x);
  int yn = yPos(y);
  string quad;
  switch(player){
    case 1:
     quad = "images/QP1.jpg";
     break;
    case 2:
     quad = "images/QP2.jpg";
     break;
    case 3:
     quad = "images/QP3.jpg";
     break;
    case 4:
     quad = "images/QP4.jpg";
     break;
  }
  SDL_Rect Rect = {xn,yn,30,30};
  SDL_Rect Rect2 = {xn-100,yn-10,30,30};
  SDL_BlitSurface(IMG_Load(quad.c_str()), NULL, currentBoard, &Rect2);
  SDL_BlitSurface(IMG_Load(quad.c_str()), NULL, gScreenSurface, &Rect);
  SDL_UpdateWindowSurface(gWindow);
  addLep();
}

//set function for the board class to use
void Graphics::setLepPos(int p) {
  lepPos=p;
}


//moves the leprechaun the the lepPos position
void Graphics::addLep() {

  SDL_Rect rect,rect2;
  SDL_Rect r = {100,10,500,615};
  SDL_FillRect(gScreenSurface, NULL, 0);
  
  string lep = "images/lep.png";

  switch(lepPos) {
    case 2:
      rect = {381,400,75,75};   
      break;
    case 3:
      rect = {256,400,75,75};
      rect2 = {256,165,75,75};
      break;
    case 4:
      rect = {138,46,75,75};
      rect2 = {374,165,75,75};
      break;
    case 5:
      rect = {374,46,75,75};
      rect2 = {374,515,75,75};
      break;
    case 6:
      rect = {138,515,75,75};
      rect2 = {489,515,75,75};
      break;
    case 8:
      rect = {138,165,75,75};
      rect2 = {489,281,75,75};
      break;
    case 9:
      rect = {489,46,75,75};
      rect2 = {489,400,75,75};
      break;
    case 10:
      rect = {138,281,75,75};
      rect2 = {489,165,75,75};
      break;
    case 11:
      rect = {256,46,75,75};
      rect2 = {256,281,75,75};
      break;
    case 12:
      rect = {256,515,75,75};
      break;
    default:
      rect = {374,281,75,75};
      rect2 = {138,400,75,75};
     
  }

  SDL_BlitSurface(currentBoard, NULL, gScreenSurface, &r); 
  SDL_BlitSurface(IMG_Load(lep.c_str()),NULL, gScreenSurface, &rect);
  SDL_BlitSurface(IMG_Load(lep.c_str()),NULL, gScreenSurface, &rect2);
   SDL_UpdateWindowSurface( gWindow);

}


//helper function to draw in the right location depending where they click
int Graphics::xPos(int x) {

  if(x>103&&x<133){
    return 103;}
  else if(x>218&&x<248){
    return 218; }
  else if(x>335&&x<365){
    return 335;}
  else if(x>451&&x<481){
    return 451;}
  else if(x>567&&x<597){
    return 567;}
  else
    return 0;

}

//helper function to draw in the right location
int Graphics::yPos(int y) {

  if(y>13&&y<43){
    return 13;}
  else if(y>126&&y<156){
    return 126;}
  else if(y>246&&y<276){
   return 246;}
  else if(y>359&&y<389){
   return 359;}
  else if(y>475&&y<505){
   return 475;}
  else if(y>593&&y<623){
   return 593;}
  else
    return 0;


}

//helper function for sidewalk location determination
int Graphics::xPos2(int x) {

  if(x>140&&x<209)
    return 145;
  else if(x>251&&x<327)
    return 256;
  else if(x>368&&x<449)
    return 373;
  else if(x>487&&x<562)
    return 492;
  else
    return 0;

}

//helper function for sidewalk location determination
int Graphics::yPos2(int y) {

  if(y>50&&y<125)
    return 55;
  else if(y>162&&y<240)
    return 167;
  else if(y>276&&y<354)
   return 281;
  else if(y>393&&y<468)
   return 398;
  else if(y>512&&y<588)
   return 517;
  else
    return 0;

}

//destroy the graphics resources and exits
void Graphics::close() {
  for (int i = 0; i < TOTAL; i++) {
    SDL_FreeSurface(gScreens[i]);
    gScreens[i] = NULL;
  }

  SDL_DestroyWindow(gWindow);
  gWindow = NULL;
  
  IMG_Quit();
  SDL_Quit();
  exit(0);
}

bool Graphics::initialize() {

  //Screen dimension constants
  const int SCREEN_WIDTH = 700;
  const int SCREEN_HEIGHT = 650;
 
  //initialize the graphics
  if( SDL_Init( SDL_INIT_VIDEO)<0){ 
    cout << "error initializing" << endl;
    return false;}
  else {
    gWindow=SDL_CreateWindow("Settlers of Notre Dame",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,SCREEN_WIDTH,SCREEN_HEIGHT,SDL_WINDOW_SHOWN);
    if(gWindow==NULL){
      cout << "error creating window" << endl;
      return false;}
    else {
      int imgFlags = IMG_INIT_JPG;
      if(!( IMG_Init( imgFlags ) & imgFlags )){ 
        cout << "error initilizing images" << endl; return false;}
      else{
        gScreenSurface = SDL_GetWindowSurface( gWindow );
        //load each of the initial images into a screen
        string dome = "images/Title_Dome.jpg";
        string stadium = "images/Title_Stadium.jpg";
        string board = "images/Board.jpg";
        string rules = "images/rules.jpg";
        gScreens[RULES] = IMG_Load(rules.c_str());
        gScreens[DOME] = IMG_Load(dome.c_str());
        gScreens[MENU] = IMG_Load(stadium.c_str());
        gScreens[BOARD] = IMG_Load(board.c_str());
        if(gScreens[RULES]==NULL||gScreens[DOME]==NULL||gScreens[MENU]==NULL||gScreens[BOARD]==NULL){
          cout << "Failed to load all images." << endl;
          return false;}
        currentBoard = gScreens[BOARD];

        //display menu and display until play game is pressed
        SDL_Event e;
        SDL_Rect srcRect = {0,0,700,500};
        int x,y;
        bool menu=true;
        gCurrentSurface = gScreens[DOME];
        SDL_FillRect(gScreenSurface, NULL, 0);      
        SDL_BlitSurface( gCurrentSurface, NULL, gScreenSurface, &srcRect);
        SDL_UpdateWindowSurface( gWindow);
        while(menu){
          while(SDL_PollEvent(&e) != 0) {
            if(e.type == SDL_QUIT) {
              for(int i=0;i<TOTAL;i++) {
                SDL_FreeSurface(gScreens[i]);
                gScreens[i]=NULL;
              }
            SDL_DestroyWindow(gWindow);
	    IMG_Quit(); SDL_Quit();
            gWindow=NULL;
            menu=false;
          }
          else if( e.type == SDL_KEYDOWN ){
            gCurrentSurface = gScreens[MENU];
            srcRect = {0,0,700,500};
            SDL_FillRect(gScreenSurface, NULL, 0);      
            SDL_BlitSurface( gCurrentSurface, NULL, gScreenSurface, &srcRect);
            SDL_UpdateWindowSurface( gWindow);
          }
          else if(e.type == SDL_MOUSEBUTTONDOWN&&gCurrentSurface!=gScreens[DOME]) {
            SDL_GetMouseState(&x,&y);
              //play game button, load board and quit
              if(x>229 && x<463 && y>176 && y<230){
                  srcRect = {100,10,500,615};
                  gCurrentSurface = gScreens[BOARD];
                  SDL_FillRect(gScreenSurface, NULL, 0);      
                  SDL_BlitSurface( gCurrentSurface, NULL, gScreenSurface, &srcRect);
                  SDL_UpdateWindowSurface( gWindow);
                  addLep();

                  menu = false;
               }
               //Rules button, changes screen
               else if(x>229 && x<463 && y>276 && y<330){
                  srcRect = {100,10,500,615};
                  gCurrentSurface = gScreens[RULES];
    		  SDL_FillRect(gScreenSurface, NULL, 0);      
          	  SDL_BlitSurface( gCurrentSurface, NULL, gScreenSurface, &srcRect);
          	  SDL_UpdateWindowSurface( gWindow);

               }
               //Close button, close window, quit
               else if(x>229 && x<463 && y>368 && y<426){
                   SDL_DestroyWindow(gWindow);
	           IMG_Quit(); SDL_Quit();
                   gWindow=NULL;
                   for(int i=0;i<TOTAL;i++) {
                     SDL_FreeSurface(gScreens[i]);
                     gScreens[i]=NULL;
                   } 
                   menu = false;
               }              
          }
        }
      }
    }
  }
}

return true;

}



