#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "Board.h"
#include "Piece.h"
#include "Graphics.h"
#include "Player.h"

using namespace std;

Board::Board() {
  vector<Piece> tempPieceVec;
  int tempNum;
  string tempName;

  //import piece data to create the board
  string fileName = "data.txt";

  ifstream inFile;
  inFile.open(fileName.c_str());

  //read file
  while (!inFile.eof()) {
    for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 4; j++) {
	inFile >> tempNum;
	inFile >> tempName;
	Piece tempPiece = Piece(tempNum,tempName);
	tempPieceVec.push_back(tempPiece);
      }
    board.push_back(tempPieceVec); //push vec into board
    tempPieceVec.clear(); //empty vec to prepare adding next row of pieces
    }
  }

  //initalize variables to 0
  for(int i=0; i<6; i++){
    for(int j=0;j<5;j++){
      spots[i][j]=0;
    }
  }
  for(int i=0; i<11; i++)
    for(int j=0; j<5;j++)
      sidewalks[i][j]=0;

  numPlayers = 0;

  //initialize robber position
  lepPos=0;
  //initialize army card
  army=false;

}

void Board::play() {
  
  if (!display.initialize()) {
    cout << "Oh no, you cannot play!" << endl;
    return;
  }
  else {
    //request for number of players
    cout << "Please enter the number of players (2-4): ";
    cin >> numPlayers;
    while (numPlayers<2 || numPlayers>4) {
      cout << "Invalid option. Please enter a number between 2 and 4: ";
      cin >> numPlayers;
    }
    //Play first turn
    playFirst();
    
    int x,y,count=0,player;
    bool playing=true;
    int lepChoice;
    
    while(playing){ //play until winner
      for (int i = 0; i < numPlayers; i++) {
	rollDice(); //roll the dice!
	cout << "The roll is " << roll << endl;
	  if (roll == 7) { //Leprechaun roll! 
	    for (int j = 0; j < numPlayers; j++) 
	      players[j].rob(); //players lose half of each resource they have
	    //player decides where to place the leprechaun
	    cout << "Player " << i+1 << " please enter a number for the lepreachaun to be on: ";
	    cin >> lepChoice;
	    while (lepChoice<2||lepChoice>12||lepChoice==7||lepChoice==lepPos){
	      cout << "Cannot keep current number and must be between 2 and 12 (but not 7): ";
	      cin >> lepChoice;
	    }
	    lepPos = lepChoice; //update lep position
	    display.setLepPos(lepPos); //find position for lep based on number
	    display.addLep(); //draw lep onto board
	  }
	  else {
	    for (int j = 0; j < numPlayers; j++) //give cards to players based on roll
	    giveCards(j);	  
	  }
       playTurn(i);
       //check if a player has won
       if (players[i].checkWin(numPlayers)) { //player wins!
	 cout << "Congratulations, Player " << i+1 << "! You win!" << endl; 
	 playing=false; //break out of loop
	 break;
       }
      }
    }
    //Close graphics window before exiting program
    display.close();
  }
}


void Board::playFirst() {
  //for the first turn, each player takes turns placing their first dorm and sidewalk

  //place first dorm
  for(int i=1; i<=numPlayers; i++) {
    cout << "Player " << i << " it is your first turn. Please select a place to build your first dorm!" << endl;
    placeDorm(i);
    //place first (adjacent) sidewalk
    cout << "Alright, time to build a sidewalk for that dorm! Pick a spot, but make sure it's connected to your dorm!" << endl;
    placeSidewalk(i,1);  
  }
  
  //place second dorm 
  for(int i=numPlayers; i>=1; i--) {
    cout << "Cool! Player " << i << " , chose another place to build a dorm! " << endl;
    placeDorm(i);
    //place second (adjacent sidewalk)
    cout << "Awesome! Choose another location for a sidewalk! " << endl;
    placeSidewalk(i,1);
  }
}

//play normal turn given which player is playing
void Board::playTurn(int player) {
  bool finish = false;
  int choice;
  
  while (!finish) {
    choice = getOpts(player); //get choice from player using function
    
    if (choice == 1) //view building costs
      buildCost(player);
    else if (choice == 2) //view current hand
      players[player].print();
    else if (choice == 3) { //build sidewalk
      if (players[player].checkResources("sidewalk")) { //check if they have enough resources
	cout << "Choose a place for your new sidewalk! Make sure it connects to one of your current dorms or quads! Click the return key on the board if you would like to return without building." << endl;
	placeSidewalk(player+1,2);
      }
      else //not enough rseources
	cout << "You do not have enough resources to build a sidewalk!" << endl;
    }
    else if (choice == 4) { //build dorm 
      if (players[player].checkResources("dorm")) { //check if they have enough resources
	cout << "Choose a place for your new dorm! Make sure it connects to one of your sidewalks! Click the return key on the board if you would like to return without building." << endl;
	placeDorm2(player+1);
      }
      else //not enough resources
	cout << "You do not have enough resources to build a dorm!" << endl;
    }
    else if (choice == 5) { //build quad
      if (players[player].checkResources("quad")) { //check if they have enough resources
	cout << "Choose a place for your new quad! You must already have a dorm there! Click the return key on the board if you would like to return without building." << endl;
	placeQuad(player+1);
      }
      else //not enough resources
	cout << "You do not have enough resources to build a quad!" << endl;
    }
    else if (choice == 6) { //buy development card
      if(players[player].checkResources("development")) { //check if have enough resources
        players[player].removeResources("development");
        giveDevelopmentCard(player);
}
      else //not enough resources
        cout << "You do not have enough resources for a resource card!" << endl;}
    else if (choice == 7) { //trade 3 for 1
      players[player].tradeResource();
    }
    else if (choice == 8) { //display victory points of each player
      for (int n = 0; n < numPlayers; n++)
	cout << "Player " << n+1 << " victory points: " << players[n].getvpoints() << endl;
    }
    else if (choice == 9) { //finish turn
      cout << "Okay, we will move on to the next player's turn." << endl;
      finish = true;  
    }
    else { //quit
      cout << "Thanks for playing Settlers of Notre Dame!" << endl;
      display.close();
      exit(0);
    }
  }
}

//display choice options for the player and return which choice they choose
int Board::getOpts(int player) {
  int choice;
  SDL_Event e;
  cout << "Player " << player+1 << ", it is your turn! What would you like to do?" << endl;
  cout << "	1 - View building costs" << endl;
  cout << " 	2 - View current resource cards" << endl;
  cout << " 	3 - Build sidewalk" << endl;
  cout << " 	4 - Build dorm" << endl;
  cout << " 	5 - Build quad" << endl;
  cout << "	6 - Buy a development card" << endl;
  cout << " 	7 - Trade 3 resources for 1 resource" << endl;
  cout << " 	8 - Check victory point standings" << endl;
  cout << " 	9 - Pass turn" << endl;
  cout << " 	10 - Quit game" << endl;
  cout << "Please select an option: ";
  cin >> choice;
  
  while (choice < 1 || choice > 10) { //if choice is invalid
    cout << "Please select an option between 1 and 10: ";
    cin >> choice;
  }
  
  return choice;
}

//dorm placing function for the first turn specifically
void Board::placeDorm(int player) {
  SDL_Event e;
  bool valid=false;
  int x=0, y=0;

  //wait for user to choose a spot to place dorm
  while(!valid){
    while(SDL_PollEvent(&e) !=0) {
      if (e.type == SDL_QUIT) {
	display.close();
	return;
      }
      else if (e.type == SDL_KEYDOWN) {
       cout << "Skipped building" << endl; return;}
      else if(e.type == SDL_MOUSEBUTTONDOWN){
	  SDL_GetMouseState(&x,&y);
	  valid = checkSpotD(x,y,player,1); //make sure click is in correct spot
	  break;
        }
      }
    }
  
  display.addDorm(x,y,player); //add dorm to board
  players[player-1].addpoints(); //add victory point to player 
}

//dorm placing function for normal turns
void Board::placeDorm2(int player) {
  SDL_Event e;
  bool valid=false;
  int x=0, y=0;

  while(!valid){
    while(SDL_PollEvent(&e) !=0) {
      if (e.type == SDL_QUIT) {
        display.close();
        return;
      }
      else if (e.type == SDL_KEYDOWN) {
       cout << "Skipped building" << endl; return;}
      else if(e.type == SDL_MOUSEBUTTONDOWN){
        SDL_GetMouseState(&x,&y);
        valid = checkSpotD(x,y,player,2); //make sure click is in correct spot
        break;}
    }
  }
  display.addDorm(x,y,player); //add dorm to board
  players[player-1].addpoints(); //add victory point to player
  players[player-1].removeResources("dorm");
}

//get location and place sidewalk
void Board::placeSidewalk(int player, int turn) {
  SDL_Event e;
  bool valid=false;
  int x=0, y=0;
  
  while(!valid){
    while(SDL_PollEvent(&e) !=0) {
      if (e.type == SDL_QUIT) {
	display.close();
	return;
      }
      else if (e.type == SDL_KEYDOWN) {
       cout << "Skipped building" << endl; return;}
      else if(e.type == SDL_MOUSEBUTTONDOWN){
	SDL_GetMouseState(&x,&y);
	valid = checkSpotS(x,y,player); //make sure click is in valid spot
	break;}
    }
  }
  display.addSide(x,y,player); //add sidewalk to board
  if(turn!=1)
    players[player-1].removeResources("sidewalk");
}

void Board::placeQuad(int player) {
  SDL_Event e;
  bool valid=false;
  int x=0, y=0;

  while(!valid){
    while(SDL_PollEvent(&e) !=0) {
      if (e.type == SDL_QUIT) {
        display.close();
        return;
      }
      else if (e.type == SDL_KEYDOWN) {
       cout << "Skipped building" << endl; return;}
      else if(e.type == SDL_MOUSEBUTTONDOWN){
        SDL_GetMouseState(&x,&y);
        valid = checkSpotQ(x,y,player);
        break;}
    }
  }
  display.addQuad(x,y,player); //add quad to board
  players[player-1].addpoints(); //add victory point to player
  players[player-1].removeResources("quad");
}

//make sure spot clicked is valid for quad (must be on dorm of current player)
bool Board::checkSpotQ(int x, int y, int player) {
  int xpos=0, ypos=0;

  if(x>103&&x<133)
    xpos=0;
  else if(x>218&&x<248)
    xpos=1;
  else if(x>335&&x<365)
    xpos=2;
  else if(x>451&&x<481)
    xpos=3;
  else if(x>567&&x<597)
    xpos=4;
  else{
    cout << "That is an invalid location, please select a different location" << endl;
    return false;
  }

  if(y>13&&y<43)
    ypos=0;
  else if(y>126&&y<156)
    ypos=1;
  else if(y>246&&y<276)
   ypos=2;
  else if(y>359&&y<389)
   ypos=3;
  else if(y>475&&y<505)
   ypos=4;
  else if(y>593&&y<623)
   ypos=5;
  else{
    cout << "That is an invalid location, please select a different location" << endl;
    return false;
  }

  if(spots[ypos][xpos]==player){
    updatePieces(ypos,xpos,player);
    return true;}
  cout << "You don't have a dorm there, please select a different location" << endl;   

}

//make sure spot clicked is valid for dorm 
//must be on next to sidewalk of current player for turn = 2
//can be anywhere if turn = 1
bool Board::checkSpotD(int x, int y, int player, int turn) {
  int xpos=0, ypos=0;

  if(x>103&&x<133)
    xpos=0;
  else if(x>218&&x<248)
    xpos=1;
  else if(x>335&&x<365)
    xpos=2;
  else if(x>451&&x<481)
    xpos=3;
  else if(x>567&&x<597)
    xpos=4;
  else{
    cout << "That is an invalid location, please select a different location" << endl;
    return false; 
  }

  if(y>13&&y<43)
    ypos=0;
  else if(y>126&&y<156)
    ypos=1;
  else if(y>246&&y<276)
   ypos=2;
  else if(y>359&&y<389)
   ypos=3;
  else if(y>475&&y<505)
   ypos=4;
  else if(y>593&&y<623)
   ypos=5;
  else{
    cout << "That is an invalid location, please select a different location" << endl;
    return false;
  }

  if(spots[ypos][xpos]==0){
    if(turn==1){
      spots[ypos][xpos]=player;
      updatePieces(ypos,xpos,player);
      return true;}
    else if((ypos*2-1)>=0&&sidewalks[ypos*2-1][xpos]==player){
      spots[ypos][xpos]=player;
      updatePieces(ypos,xpos,player);
      return true;}
    else if((ypos*2+1)<11&&sidewalks[ypos*2+1][xpos]==player){
      spots[ypos][xpos]=player;
      updatePieces(ypos,xpos,player);
      return true;}
    else if((ypos*2)<11&&sidewalks[ypos*2][xpos]==player){
      spots[ypos][xpos]=player;
      updatePieces(ypos,xpos,player);
      return true;}
    else if((ypos*2)<11&&(xpos-1)>=0&&sidewalks[ypos*2][xpos-1]){
      spots[ypos][xpos]=player;
      updatePieces(ypos,xpos,player);
      return true;}
    else{
      cout << "The dorm must be next to a sidewalk" << endl;
      return false;}
   }
  
  
  cout << "There is already a dorm there, pick somewhere else!" << endl; 
  return false;

}

//make sure spot clicked is in valid for sidewalk
bool Board::checkSpotS(int x, int y, int player) {

  int xpos, ypos;
  //check if horizontal or vertical (x)
  if(x>103&&x<133)
    xpos=0;
  else if(x>218&&x<248)
    xpos=1;
  else if(x>335&&x<365)
    xpos=2;
  else if(x>451&&x<481)
    xpos=3;
  else if(x>567&&x<597)
    xpos=4;
  else{
    xpos=-1;
  }

  //check if horizontal or vertical (y)
  if(y>13&&y<43)
    ypos=0;
  else if(y>126&&y<156)
    ypos=2;
  else if(y>246&&y<276)
   ypos=4;
  else if(y>359&&y<389)
   ypos=6;
  else if(y>475&&y<505)
   ypos=8;
  else if(y>593&&y<623)
   ypos=10;
  else
    ypos=-1;

  //if vertical
  if(xpos!=-1&&ypos==-1) {
   if(y>50&&y<125)
    ypos=1;
  else if(y>162&&y<240)
    ypos=3;
  else if(y>276&&y<354)
    ypos=5;
  else if(y>393&&y<468)
    ypos=7;
  else if(y>512&&y<588)
    ypos=9;
  else {
    cout << "Not a valid sidewalk location, try again!" << endl;
    return false;}
  
    if(sidewalks[ypos][xpos]==0){ //check sidewalk is next to a dorm
      if(spots[(ypos-1)/2][xpos]==player||spots[int(double(ypos)/2+0.5)][xpos]==player){
	sidewalks[ypos][xpos]=player;
	return true;}
      else{
       cout << "Uh oh! Remember it needs to be next to one of your dorms!" << endl;
       return false;}
    }
  }
  //horizontal
  else if(xpos==-1&&ypos!=-1) {
    if(x>140&&x<209)
      xpos = 0;
    else if(x>251&&x<327)
      xpos = 1;
  else if(x>368&&x<449)
      xpos = 2;
  else if(x>487&&x<562)
      xpos = 3;
  else{
    cout << "Not a valid sidewalk location, try again!" << endl;
    return false;}
    if(sidewalks[ypos][xpos]==0){ //check sidewalk is next to a dorm
      if(spots[ypos/2][xpos]==player||spots[ypos/2][xpos+1]==player){
	sidewalks[ypos][xpos]=player;
	return true;}
      else{
	cout << "Uh oh! Remember it needs to be next to one of your dorms!" << endl;
	return false;
      }
    }
  }
  cout << "Not a valid sidewalk location, try again!" << endl;
  return false; 

}

//allocate pieces to players based on their dorm location
void Board::updatePieces(int y, int x, int player) {

  player=player-1;
  
  if ( (y-1)>=0 && (x-1)>=0 ) { 
    players[player].build(board[y-1][x-1]);
    players[player].numPieces++;
  }
  if ( (y-1)>=0 && x>=0 && x<4 ) { 
    players[player].build(board[y-1][x]);
    players[player].numPieces++;
  }
  if ( y>=0 && y<5 && (x-1)>=0 ) {
    players[player].build(board[y][x-1]);
    players[player].numPieces++;
  }
  if ( y>=0 && y<5 && x>=0 && x<4 ) {
    players[player].build(board[y][x]);
    players[player].numPieces++;
  }
}

//helper function displays spots array
void Board::displaySpots() {
  for(int i=0; i<6; i++){
    for(int j=0;j<5;j++){
      cout << spots[i][j] << " ";
    }
    cout << endl;
  }
}

//helper function displays sidewalks array
void Board::displaySidewalks() {
  for(int i=0; i<11; i++) {
    for(int j=0; j<5;j++)
      cout << sidewalks[i][j];
    cout << endl;
  }
}

//roll the dice
void Board::rollDice() {
  srand(time(NULL));
  int die1, die2;
  
  //roll die 1
  die1 = rand() % 6 + 1;
  //roll die 2
  die2 = rand() % 6 + 1;

  roll = die1+die2;
}

//give resource cards based on players' pieces and the roll
void Board::giveCards(int player) {
  if(roll!=lepPos) 
    players[player].hasPiece(roll);
}

//display building costs to user
void Board::buildCost(int player) {
  cout << "To build a sidewalk, you need 1 bicep, 1 test, and 1 berry." << endl;
  cout << "To build a dorm, you need 1 test, 1 bicep, 1 helmet, and 1 berry." << endl;
  cout << "To build a quad, you need 3 suns and 2 helmets." << endl;
  cout << "To buy a development card, you need 1 sun, 1 berry, and 1 helmet." << endl;
}

//give development card to user if purchased
void Board::giveDevelopmentCard(int player){
  srand(time(NULL));
  string card; 
  int lepChoice;
  int opt; 
  int num = rand() % 25 +1, count=0; //assign card based on probabilities.
  if (num>=15 && num <20) opt =1;
  else if (num <15) opt =2;
  else if (num >=20 && num<22) opt =3;
  else if (num >= 22 && num<24) opt =4;
  else opt = 5;
  switch(opt) {
    case 1: //Victory Point card adds a victory point to the player
      cout << "You got a Victory Point card! Your point has been added to your total. " << endl;
      players[player].addpoints();
      cout << "You now have " << players[player].getvpoints() << " Victory Points!" << endl;
      break;
    case 2: //Leprechaun card allows user to move the leprechaun to a new number - build up largest army
      cout << "You got a Leprechaun card! Please chose a new location for the leprechaun: ";
      cin >> lepChoice;
      while(lepChoice<2||lepChoice>12||lepChoice==7){ //if invalid choice
       cout << "Must be between 2 and 12 (but not 7, and you can keep leprechaun in the same place): ";
       cin >> lepChoice;
      }
      lepPos = lepChoice;
      display.setLepPos(lepPos); //find spot for leprechaun
      display.addLep(); //add leprechaun to the board
      players[player].addLep(); //add leprechaun to players army
      if (players[player].ifLep() == 0 && players[player].numLep() >= 3){ //if you don't have largest army and you could qualify
        if (army) { // if someone else has largest army
          for (int i = 0; i<numPlayers; i++){ // find player with largest army
            if (players[i].ifLep() == 1 && players[i].numLep() < players[player].numLep()){ // change possesion of this card if current player has larger army
              players[i].setLep(false); // remove largest army card
              players[player].setLep(true); // give player largest army card
              cout << "You have the largest army! Thats one victory point; until someone over takes you" << endl;
            }
          }
        }
        else{ //no one else has largest army
         players[player].setLep(true); // this player has largest army
         army = true; // army card has been given
         cout << "You have the largest army! Thats one victory point; until someone over takes you" << endl;
       }
      }
      break;
    case 3: //Year of Plenty card gives two resource cards of the player's choice
      cout << "You got a Year of Plenty card! Which resource would you like another one of (berry, bicep, helmet, sun, or test)? ";
      cin >> card;
      while(card!="berry"&&card!="sun"&&card!="helmet"&&card!="test"&&card!="bicep"){ //if choice is invalid
        cout << "That's not a valid resource, try again: ";
        cin >> card;}
      players[player].draw(card); //give card of choice
      cout << "You get one more. Please enter which you would like: ";
      cin >> card;
      while(card!="berry"&&card!="sun"&&card!="helmet"&&card!="test"&&card!="bicep"){ //if choice is invalid
        cout << "That's not a valid resource, try again: ";
        cin >> card;}
      players[player].draw(card); //give card of choice
      cout << "Here are your new card counts! " << endl;
      players[player].print();
      break;
    case 4: //Monopoly card allows you to steal all of one resource from the other players
     cout << "You got a Monopoly card! You can take all of one resource from the other players! Which do you want (berry, bicep, helmet, sun, or test)? ";
     cin >> card;
     while(card!="berry"&&card!="sun"&&card!="helmet"&&card!="test"&&card!="bicep"){ //if choice not valid
       cout << "That's not a valid resource, try again: ";
       cin >> card;}
     for(int i = 0; i<numPlayers;i++) { //steal resource from all players
       if(i!=player) { //don't steal from self
         count+=players[i].stealCards(card);
       }
     }
     for(int k = 0; k<count; k++) //give cards to player
       players[player].draw(card);
    cout << "Here are your new card counts! " << endl;
      players[player].print();
      break;
    case 5: //Road Building card builds sidewalk for free
      cout << "You got a Road Building card! Please click a location to build a sidewalk! If you have nowhere to build, press enter on the board screen." << endl;
      placeSidewalk(player+1,1);
      break;
  }
}
