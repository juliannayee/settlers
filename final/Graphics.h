//Using SDL, SDL_image, standard IO, and strings
#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <iostream>
#include "Player.h"
#include <vector>
#include "Piece.h"
#include <string>
using namespace std;


class Graphics {
  public:
    Graphics(); //set variables
    void addDorm(int,int,int); //update board to add dorm
    void addSide(int,int,int); //update board to add sidewalk
    void addQuad(int,int,int);
    void addLep(); //update board to add quad
    void setLepPos(int);
    void close(); //close graphics window, free space
    bool initialize(); //initialize the window 
  private:
    SDL_Window* gWindow;
    SDL_Surface* gScreenSurface;
    SDL_Surface* gCurrentSurface;
    SDL_Surface* currentBoard;
    enum gScreens { DOME, MENU, BOARD, RULES, TOTAL};
    SDL_Surface* gScreens[TOTAL];
    int lepPos;
    //helper functions to determine valid locations
    int xPos(int);
    int yPos(int);
    int xPos2(int);
    int yPos2(int);

};


#endif
