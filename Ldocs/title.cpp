//  Display title & menu screens, Julianna Yee
//  Code displays the title image; once user presses "return key", the
//  image changes to one of the menu screen

//Using SDL, SDL_image, standard IO, and strings
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <iostream>
#include <string>
using namespace std;

//Screen dimension constants
const int SCREEN_WIDTH = 700;
const int SCREEN_HEIGHT = 650;

enum KeyPressSurfaces {
  KEY_PRESS_SURFACE_DOME,
  KEY_PRESS_SURFACE_MENU,
  KEY_PRESS_SURFACE_BOARD,
  KEY_PRESS_SURFACE_RULES,
  KEY_PRESS_SURFACE_DORM,
  KEY_PRESS_SURFACE_SIDEV,
  KEY_PRESS_SURFACE_SIDEH,
  KEY_PRESS_SURFACE_TOTAL
};



//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image
SDL_Surface* loadSurface( std::string path );

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The surface contained by the window
SDL_Surface* gScreenSurface = NULL;

//Current displayed JPG image
//SDL_Surface* gJPGSurface = NULL;

//Images that correspond to a keypress
SDL_Surface* gKeyPressSurfaces[ KEY_PRESS_SURFACE_TOTAL ];

//Current displayed image
SDL_Surface* gCurrentSurface = NULL;


//
int xPos(int);
int yPos(int);
int xPos2(int);
int yPos2(int);

bool init() {
    //Initialization flag
    bool success = true;
    
    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
        success = false;
    }
    else {
        //Create window
        gWindow = SDL_CreateWindow( "Settlers of Notre Dame", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( gWindow == NULL ) {
            cout << "Window could not be created! SDL_Error: " << SDL_GetError() << endl;
            success = false;
        }
        else {
            //Initialize JPG loading
            int imgFlags = IMG_INIT_JPG;
            if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
                cout << "SDL_image could not initialize! SDL_image Error: " << SDL_GetError() << endl;
                success = false;
            }
            else {
                //Get window surface
                gScreenSurface = SDL_GetWindowSurface( gWindow );
            }
        }
    }
    
    return success;
}

bool loadMedia() {
    //Loading success flag
    bool success = true;
    
    //Load default JPG surface
    gKeyPressSurfaces[KEY_PRESS_SURFACE_DOME] = loadSurface( "images/Title_Dome.jpg" );
    if( gKeyPressSurfaces[KEY_PRESS_SURFACE_DOME] == NULL ) {
        cout << "Failed to load default image!" << endl;
        success = false;
    }
    
    //Load next surface
    gKeyPressSurfaces[KEY_PRESS_SURFACE_MENU] = loadSurface("images/Title_Stadium.jpg");
    if (gKeyPressSurfaces[KEY_PRESS_SURFACE_MENU] == NULL) {
        cout << "Failed to load stadium image!" << endl;
        success = false;
    }
    gKeyPressSurfaces[KEY_PRESS_SURFACE_BOARD] = loadSurface("images/Board.jpg");
    if (gKeyPressSurfaces[KEY_PRESS_SURFACE_BOARD] == NULL) {
        cout << "Failed to load board image!" << endl;
        success = false;
    }    
    gKeyPressSurfaces[KEY_PRESS_SURFACE_RULES] = loadSurface("images/rules.jpg");
    if (gKeyPressSurfaces[KEY_PRESS_SURFACE_RULES] == NULL) {
        cout << "Failed to load rules image!" << endl;
        success = false;
    }

   gKeyPressSurfaces[KEY_PRESS_SURFACE_DORM] = loadSurface("images/dorm.jpg");
   if (gKeyPressSurfaces[KEY_PRESS_SURFACE_DORM] == NULL) {
        cout << "Failed to load dorm image!" << endl;
        success = false;
    }

   gKeyPressSurfaces[KEY_PRESS_SURFACE_SIDEV] = loadSurface("images/P4sidev.jpg");
   if (gKeyPressSurfaces[KEY_PRESS_SURFACE_SIDEV] == NULL) {
        cout << "Failed to load sidev image!" << endl;
        success = false;
    }

   gKeyPressSurfaces[KEY_PRESS_SURFACE_SIDEH] = loadSurface("images/P4sideh.jpg");
   if (gKeyPressSurfaces[KEY_PRESS_SURFACE_SIDEH] == NULL) {
        cout << "Failed to load sideh image!" << endl;
        success = false;
    }



    return success;
}

void close() {
    //Free loaded image
    for (int i = 0; i < KEY_PRESS_SURFACE_TOTAL; i++) {
        SDL_FreeSurface( gKeyPressSurfaces[i] );
        gKeyPressSurfaces[i] = NULL;
    }
    
    //Destroy window
    SDL_DestroyWindow( gWindow );
    gWindow = NULL;
    
    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();
}

SDL_Surface* loadSurface( std::string path )
{
    //The final optimized image
    SDL_Surface* optimizedSurface = NULL;
    
    //Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
    if( loadedSurface == NULL ) {
        cout << "Unable to load image " << path.c_str() << "! SDL Error: " << SDL_GetError() << endl;
    }
    else {
        //Convert surface to screen format
        optimizedSurface = SDL_ConvertSurface( loadedSurface, gScreenSurface->format, NULL );
        if( optimizedSurface == NULL ) {
            cout << "Unable to optimize image " << path.c_str() << "! SDL Error: " << SDL_GetError() << endl;
        }
        
        //Get rid of old loaded surface
        SDL_FreeSurface( loadedSurface );
    }
    
    return optimizedSurface;
}

int main( int argc, char* args[] )
{
    //Start up SDL and create window
    if( !init() ) {
        cout << "Failed to initialize!" << endl;
    }
    else {
        //Load media
        if( !loadMedia() ) {
            cout << "Failed to load media!" << endl;
        }
        else {
            //Main loop flag
            bool menu = false;           
            //Event handler
            SDL_Event e;
            //Set default current surface
            gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DOME];            SDL_Rect srcRect = {0,0,700,500};
            int x,y;
            //While menu is running
            while( !menu ) {
                //Handle events on queue
                while( SDL_PollEvent( &e ) != 0 ) {
                    //User requests quit
                    if( e.type == SDL_QUIT ) {
                        close();
                        return 0;
                    }
                    //User presses a key
                    else if( e.type == SDL_KEYDOWN) {
                      gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_MENU];
                      srcRect = {0,0,700,500};
                    }
                    else if(e.type == SDL_MOUSEBUTTONDOWN&&gCurrentSurface!=gKeyPressSurfaces[KEY_PRESS_SURFACE_DOME]) {
                         SDL_GetMouseState(&x,&y);
                         cout << x << "  " << y << endl;

 			if(x>229 && x<463 && y>176 && y<230){
                          srcRect = {100,10,500,615};
 			  gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_BOARD];
                          menu = true;       
                         }
                        else if(x>229 && x<463 && y>276 && y<330){
                          srcRect = {100,10,500,615};
                          gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_RULES];}
                        else if(x>229 && x<463 && y>368 && y<426){
 		           close();
                           return 0;}
 			}

                    //Apply the JPG image
                    SDL_FillRect(gScreenSurface, NULL, 0);
                    SDL_BlitSurface( gCurrentSurface, NULL, gScreenSurface, &srcRect);
                    
                    //Update the surface
                    SDL_UpdateWindowSurface( gWindow );            }          }      
    
   int xn,yn,x2,y2;
   bool quit = false;
   //board functions
   while (!quit) {
     while( SDL_PollEvent( &e ) != 0 ) { 
       if( e.type == SDL_QUIT )
          quit = true;

       else if(e.type == SDL_MOUSEBUTTONDOWN) {
         
         SDL_GetMouseState(&x,&y);
         cout << x << "  " << y << endl;
         cout << xn << " new " << yn << endl;
         xn = xPos(x); //returns 0 if invalid x
         yn = yPos(y); //returns 0 if invalid y
         if(xn>0&&yn>0){
           SDL_Rect Rect = {xn,yn,30,30};
           SDL_BlitSurface(gKeyPressSurfaces[KEY_PRESS_SURFACE_DORM], NULL, gScreenSurface, &Rect);
           SDL_UpdateWindowSurface(gWindow);
         }
         else if(xn>0&&yn==0){ //vertical sidewalk
           y2 = yPos2(y);
           if(xn>0&&y2>0){ 
             SDL_Rect Rect = {xn+5,y2,15,50};
             SDL_BlitSurface(gKeyPressSurfaces[KEY_PRESS_SURFACE_SIDEV], NULL, gScreenSurface, &Rect);
             SDL_UpdateWindowSurface(gWindow);
           }
         }
         else if(xn==0&&yn>0){  //horizontal
             x2 = xPos2(x);
           if(x2>0&&yn>0){
             SDL_Rect Rect = {x2,yn+5,50,15};
             SDL_BlitSurface(gKeyPressSurfaces[KEY_PRESS_SURFACE_SIDEH], NULL, gScreenSurface, &Rect);
             SDL_UpdateWindowSurface(gWindow);
           }

         }
        
         
      }         


     }

   }

} }
    //Free resources and close SDL
    close();
    
    return 0;
}



int xPos(int x) {

  if(x>103&&x<133)
    return 103;
  else if(x>218&&x<248)
    return 218;
  else if(x>335&&x<365)
    return 335;
  else if(x>451&&x<481)
    return 451;
  else if(x>567&&x<597)
    return 567;
  else
    return 0;

}

int yPos(int y) {
 
  if(y>13&&y<43)
    return 13;
  else if(y>126&&y<156)
    return 126;
  else if(y>246&&y<276)
   return 246;
  else if(y>359&&y<389)
   return 359;
  else if(y>475&&y<505)
   return 475;
  else if(y>593&&y<623)
   return 593;
  else 
    return 0;

}

int xPos2(int x) {

  if(x>140&&x<209)
    return 145;
  else if(x>251&&x<327)
    return 256;
  else if(x>368&&x<449)
    return 373;
  else if(x>487&&x<562)
    return 492;
  else
    return 0;

}

int yPos2(int y) {

  if(y>50&&y<125)
    return 55;
  else if(y>162&&y<240)
    return 167;
  else if(y>276&&y<354)
   return 281;
  else if(y>393&&y<468)
   return 398;
  else if(y>512&&y<588)
   return 517;
  else
    return 0;

}

