//Laura Syers Lab 8 
//card.cpp
//This program will display one of five resource cards

//Using SDL, SDL_image, standard IO, and strings
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>

//Screen dimensions
const int SCREEN_WIDTH = 362;
const int SCREEN_HEIGHT = 452;
//count for flip
int count = 0;
char cardName[] = "images/berriesCard.jpg";

enum KeyPressSurfaces {

  KEY_PRESS_SURFACE_DEFAULT,
  KEY_PRESS_SURFACE_LEFT,
  KEY_PRESS_SURFACE_RIGHT,
  KEY_PRESS_SURFACE_TOTAL
};


//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image
SDL_Surface* loadSurface( std::string path );

//The window we'll be rendering to
SDL_Window* gWindow = NULL;
	
//The surface contained by the window
SDL_Surface* gScreenSurface = NULL;

SDL_Surface* gKeyPressSurfaces[ KEY_PRESS_SURFACE_TOTAL ];


//Current displayed JPG image
SDL_Surface* gCardSurface = NULL;

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Create window
		gWindow = SDL_CreateWindow( "Card Display", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Initialize JPG loading
			int imgFlags = IMG_INIT_JPG;
			if( !( IMG_Init( imgFlags ) & imgFlags ) )
			{
				printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
				success = false;
			}
			else
			{
				//Get window surface
				gScreenSurface = SDL_GetWindowSurface( gWindow );
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ] = loadSurface( "images/nd.jpg");
	  if(gKeyPressSurfaces [ KEY_PRESS_SURFACE_DEFAULT] == NULL) {
	    printf("Failed to load defualt image!\n");
	    success=false; }

        gKeyPressSurfaces[ KEY_PRESS_SURFACE_LEFT ] = loadSurface( "images/nd.jpg");
          if(gKeyPressSurfaces [ KEY_PRESS_SURFACE_LEFT] == NULL) {
            printf("Failed to load defualt image!\n");
            success=false; }

        gKeyPressSurfaces[ KEY_PRESS_SURFACE_RIGHT ] = loadSurface(cardName);
          if(gKeyPressSurfaces [ KEY_PRESS_SURFACE_RIGHT] == NULL) {
            printf("Failed to load defualt image!\n");
            success=false; }


	return success;
}

void close()
{
	
	
	//Free loaded image
	for(int i =0; i < KEY_PRESS_SURFACE_TOTAL; i++) {
		SDL_FreeSurface( gKeyPressSurfaces[i] );
		gKeyPressSurfaces [i]= NULL; }

	//Destroy window
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

SDL_Surface* loadSurface( std::string path )
{
	//The final optimized image
	SDL_Surface* optimizedSurface = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Convert surface to screen format
		optimizedSurface = SDL_ConvertSurface( loadedSurface, gScreenSurface->format, NULL );
		if( optimizedSurface == NULL )
		{
			printf( "Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	return optimizedSurface; 
	


}

int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;
			gCardSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ];

			//While application is running
			while( !quit )
			{
				//Handle events on queue
				while( SDL_PollEvent( &e ) != 0 )
				{
					//User requests quit
					if( e.type == SDL_QUIT )
					{
						quit = true;
					}
					
					else if(e.type == SDL_KEYDOWN) {
						switch(e.key.keysym.sym) {

							case SDLK_LEFT:
							count++;
							if(count%2==0)
								gCardSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_LEFT ];
							else
								gCardSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_RIGHT ];
							break;

							case SDLK_RIGHT:
							count++;
							if(count%2!=0)
								gCardSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_RIGHT ];
							else
								gCardSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_LEFT];
							break;

							default:
							gCardSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ];
							break;


						}
					}
				}

				//Apply the JPG image
				SDL_BlitSurface( gCardSurface, NULL, gScreenSurface, NULL );
			
				//Update the surface
				SDL_UpdateWindowSurface( gWindow );
			}
		}
	}

	//Free resources and close SDL
	close();

	return 0;
}
