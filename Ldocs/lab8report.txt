Laura Syers Lab 8 Report
3/22/16

Bitbucket account: 
  https://bitbucket.org/peprogrammers/settlers/

Time log:

  The time I have spent so far is as follows:
	3/2/16: Found relevant lazy foo SDL tutorials and created rubric (1 hr)
	3/15/16: Started working through SDL tutorials (1 hr)
	3/16/16: Continued working on SDL tutorials in lab (1 hr)
	3/22/16: Finished tutorials on rubric and made deliverable card.cpp (5 hrs)

card.cpp: (g++ card.cpp -lSDL2 -lSDL2_image -o card)    
  This program takes a card name (changed at the beginning with variable cardName) and displays
the back of the card first. Then, when it's flipped (left or right key), it shows the card side. 
If pressed again it flipped back. The program is able to complete this by creating an array that
stores the 2 different images. Then, it initializes the media, and when it is loading, it loads
the two different images into the array. In the main, it uses SDL_Event to keep track of what is being
pressed. In this program, it quits when closed as well as switching between the surfaces whenever
a left or right key is pressed. The screen is closed when it was quit and frees the resources. 
The program was then run and it displayed the correct images with the correct functionality.
  


What was learned:
  Going through the various SDL tutorials was very helpful and helped solidify ideas for what 
to implement in our project. It was first exciting to get an image on the screen, and because
our project will be using a lot of different images (with the board, pieces, and cards) it will
be extremely useful. The tutorials also helped with mouse/key events which are helpful for any game.
Mouse events will be especially useful with our game because of the board game style. Clicking and
dragging will be needed, so I now have a start at understanding how to complete all of this.
I think I deserve all of the points for this lab because I completed all of the tutorials. 
Implementing them into the project will be the next step, but I believe I have a strong understanding
of how to do so.


