//  Display board, Julianna Yee
//  Code displays the board image; once user presses "return key"

//Using SDL, SDL_image, standard IO, and strings
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <iostream>
#include <string>
using namespace std;

//Screen dimension constants
const int SCREEN_WIDTH = 1000;
const int SCREEN_HEIGHT = 650;

//Key press surfaces constants
/*enum KeyPressSurfaces {
    KEY_PRESS_SURFACE_DEFAULT,
    KEY_PRESS_SURFACE_ENTER,
    KEY_PRESS_SURFACE_TOTAL
};*/

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image as texture
SDL_Texture* loadTexture(std::string path);

//Loads individual image
SDL_Surface* loadSurface( std::string path );

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The surface contained by the window
SDL_Surface* gScreenSurface = NULL;

//Window renderer
SDL_Renderer* gRenderer = NULL;

//Current displayed JPG image
SDL_Surface* gJPGSurface = NULL;

//Images that correspond to a keypress
//SDL_Surface* gKeyPressSurfaces[ KEY_PRESS_SURFACE_TOTAL ];

//Current displayed image
SDL_Surface* gCurrentSurface = NULL;

bool init() {
    //Initialization flag
    bool success = true;
    
    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
        success = false;
    }
    else {
        //Create window
        gWindow = SDL_CreateWindow( "Settlers of Catan", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( gWindow == NULL ) {
            cout << "Window could not be created! SDL_Error: " << SDL_GetError() << endl;
            success = false;
        }
        else {
            //Initialize JPG loading
            int imgFlags = IMG_INIT_JPG;
            if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
                cout << "SDL_image could not initialize! SDL_image Error: " << SDL_GetError() << endl;
                success = false;
            }
            else {
                //Get window surface
                gScreenSurface = SDL_GetWindowSurface( gWindow );
            }
        }
    }
    
    return success;
}

bool loadMedia() {
    //Loading success flag
    bool success = true;
    
    //Load default JPG surface
    gJPGSurface = loadSurface("images/Board.jpg");
    if ( gJPGSurface == NULL ) {
      cout << "Failed to load board image!" << endl;
      success = false;
    }
    /*gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] = loadSurface( "images/Title_Dome.jpg" );
    if( gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] == NULL ) {
        cout << "Failed to load default image!" << endl;
        success = false;
    }
    
    //Load next surface
    gKeyPressSurfaces[KEY_PRESS_SURFACE_ENTER] = loadSurface("images/Title_Stadium.jpg");
    if (gKeyPressSurfaces[KEY_PRESS_SURFACE_ENTER] == NULL) {
        cout << "Failed to load stadium image!" << endl;
        success = false;
    }*/
    
    return success;
}

void close() {
    //Free loaded image
    SDL_FreeSurface( gJPGSurface );
    gJPGSurface = NULL;
    /*for (int i = 0; i < KEY_PRESS_SURFACE_TOTAL; i++) {
        SDL_FreeSurface( gKeyPressSurfaces[i] );
        gKeyPressSurfaces[i] = NULL;
    }*/
    
    //Destroy window
    SDL_DestroyWindow( gWindow );
    gWindow = NULL;
    
    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();
}

SDL_Texture* loadTexture(std::string path) {
  //The final texture
  SDL_Texture* newTexture = NULL;

  //Load image at specified path
  SDL_Surface * loadedSurface = IMG_Load(path.c_str());
  if (loadedSurface == NULL)
    cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << endl;
  else {
    //Create texture from surface pixels
    newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
    if (newTexture == NULL)
      cout << "Unable to create texture from " << path.c_str() << "! SDL_Error: " << SDL_GetError() << endl;
    SDL_FreeSurface(loadedSurface);
  }

  return newTexture;

}

SDL_Surface* loadSurface( std::string path )
{
    //The final optimized image
    SDL_Surface* optimizedSurface = NULL;
    
    //Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
    if( loadedSurface == NULL ) {
        cout << "Unable to load image " << path.c_str() << "! SDL Error: " << SDL_GetError() << endl;
    }
    else {
        //Convert surface to screen format
        optimizedSurface = SDL_ConvertSurface( loadedSurface, gScreenSurface->format, NULL );
        if( optimizedSurface == NULL ) {
            cout << "Unable to optimize image " << path.c_str() << "! SDL Error: " << SDL_GetError() << endl;
        }
        
        //Get rid of old loaded surface
        SDL_FreeSurface( loadedSurface );
    }
    
    return optimizedSurface;
}

int main( int argc, char* args[] )
{
    //Start up SDL and create window
    if( !init() ) {
        cout << "Failed to initialize!" << endl;
    }
    else {
        //Load media
        if( !loadMedia() ) {
            cout << "Failed to load media!" << endl;
        }
        else {
            //Main loop flag
            bool quit = false;
            
            //Event handler
            SDL_Event e;
            
            //Set default current surface
            //gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];
            
            //While application is running
            while( !quit ) {
                //Handle events on queue
                while( SDL_PollEvent( &e ) != 0 ) {
                    //User requests quit
                    if( e.type == SDL_QUIT ) {
                        quit = true;
                    }
                    //User presses a key
                    /*else if( e.type == SDL_KEYDOWN) {
                        //Select surfaces based on keypress
                        switch(e.key.keysym.sym) {
                            case SDLK_RETURN:
                                gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_ENTER];
                                break;
                            default:
                                gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];
                                break;
                        }
                    }*/
		}
		//Clear screen
		SDL_SetRenderDrawColor(gRenderer, 255, 0, 0, 255);
		SDL_RenderClear(gRenderer);

		//Render red filled quad
		SDL_Rect fillRect = { 30, 30, 300, 300 };
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0x00, 0x00, 0xFF);
		SDL_RenderFillRect(gRenderer, &fillRect);
		//Apply the JPG image
		SDL_Rect srcRect = {225, 15, 500, 615};
                SDL_BlitSurface( gJPGSurface, NULL, gScreenSurface, &srcRect );
                    
                //Update the surface
		SDL_RenderPresent(gRenderer);
                SDL_UpdateWindowSurface( gWindow );
            }
        }
    }
    
    //Free resources and close SDL
    close();
    
    return 0;
}
